import { IsNotEmpty, IsInt, Min, ArrayNotEmpty } from 'class-validator';

class CreateOrderItemDao {
  @Min(0)
  @IsInt()
  @IsNotEmpty()
  productId: number;

  @Min(0)
  @IsInt()
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @Min(0)
  @IsInt()
  @IsNotEmpty()
  customerId: number;

  @ArrayNotEmpty()
  orderItems: CreateOrderItemDao[];
}
