import { IsNotEmpty, Length, IsInt, Min } from 'class-validator';

export class CreateCustomerDto {
  @Length(4, 64)
  @IsNotEmpty()
  name: string;

  @Min(0)
  @IsInt()
  @IsNotEmpty()
  age: number;

  @Length(6, 10)
  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  gender: string;
}
